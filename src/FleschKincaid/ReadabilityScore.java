/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
* I certify that all code in this file is my own work.
* This code is submitted as the solution to Assignment 1
* in CSIS44542 Object-Oriented Programming, 2017, section 03
*
* Due date: 5pm, Friday, February 17, 2017.
*
* @author Akshay Reddy Vontari
*/
package FleschKincaid;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;


public class ReadabilityScore {

    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException  {
        // initializing the variables
        int totalSentences = 0;
        int totalWords = 0;
        int totalSyllables=0;
        double readEasy = 0.0;
        double gradeLevel = 0.0;
        String schoolLevel = null;
        
         //Scanner class to read the file name
        Scanner sc = new Scanner(System.in);
        System.out.print("Please enter the name of the file containing this passage of text:");
        //name of file stored in filename using scanner method. 
        String fileName = sc.nextLine();
        //File class to extract file 
        File inputFilename = new File(fileName);
        
        
        //if statement to check whether the file exists.  
        if(inputFilename.exists())
        {
            //again,scanner to read the information from the file if file exists.
            Scanner sc1 = new Scanner(inputFilename);
            
         while(sc1.hasNext())
        {
           // logic to count the number of totalWords using string methods.
            String s = sc1.nextLine();
            String s2 = s.replaceAll("[,.?!:;-]", " ");
            String[] s1 = s2.split(" ");
            totalWords = s1.length;
            
           // logic to count the number of totalSentences using for loop and switch case.
            int c=0;
            for(int i=0;i<s.length();i++)
            {
                switch(s.charAt(i))
                {
                    case '.':
                        c=c+1;
                        break;
                    case '!':
                        c=c+1;
                        break;
                    case '?':
                        c=c+1;
                        break;
                    case ':':
                        c=c+1;
                        break;
                    case ';':
                        c=c+1;
                        break;
                    
                        
                }
                totalSentences=c;
               
            }
                // logic to count the number of syllables even if syllables are adjacent
		String x;
                x = s.replaceAll("[,.?!:;-]", " ");
                x = x.toLowerCase();
		int l = 0;
		boolean flag = true;
		for(int i = 0; i < x.length(); i++) 
                {
			if(x.charAt(i) == 'a' || x.charAt(i) == 'e'|| x.charAt(i) == 'i' || x.charAt(i) == 'o' || x.charAt(i) == 'u'|| x.charAt(i) == 'y') 
                        {
				if(flag == true)
                                {
				    l += 1;
				    flag = false;
				}
			} 
                        else
                        {
				flag = true;
			}
		}
                
            // logic to decrease count,if word ends with e
            
            String[] split = x.split(" ");
            for(String value : split)
            {
                if(value.charAt(value.length()-1) == 'e')
                {
                                 l--;
                }

            }   
             //logic to increment the count , if the word ends with e with only one syllable
            for(String value : split)
            {
                if(value.charAt(value.length()-1) == 'e')
                {
                    if(!(value.contains("a") || value.contains("i") || value.contains("o") || value.contains("u")|| value.contains("y")))
                    {
                        l++;
                    }
                }                    
                

            }
            
            
            totalSyllables = l;
	
	}
        //Flesch reading-ease score (FRES) Formula
        readEasy = 206.835-1.015*totalWords/totalSentences - 84.6*totalSyllables/totalWords;
        //Flesch–Kincaid Grade Level Formula
        gradeLevel = 0.39*totalWords/totalSentences+11.8*totalSyllables/totalWords - 15.59;
        
        // if-else to know the schoolLevel with readability score.
        if(readEasy<=100.00&&90.00<=readEasy)
        {
            schoolLevel = "5th grade";
        }
        else if(readEasy<=90.00&&80.00<=readEasy)
        {
            schoolLevel="6th grade";
        }
        else if(readEasy<=80.00&&70.00<=readEasy)
        {
            schoolLevel="7th grade";
        }
        else if(readEasy<=70.00&&60.00<=readEasy)
        {
            schoolLevel="8th & 9th grade";
        }
        else if(readEasy<=60.00&&50.00<=readEasy)
        {
            schoolLevel="10th to 12th grade";
        }
        else if(readEasy<=50.00&&30.00<=readEasy)
        {
            schoolLevel="College";
        }
        else if(readEasy<=30.00&&0.00<=readEasy)
        {
            schoolLevel="College Graduate";
        }
        
        System.out.println("The passage has the following scores:-");
        System.out.print("Flesch reading ease:");
       // print format method to format the text
        System.out.printf("%f", readEasy);
        System.out.print("("+schoolLevel+")");
        System.out.print("\nFlesch-Kincaid grade level:");
        // print format method to format the text
        System.out.printf("%f\n", gradeLevel);
           
        sc1.close();
        
        }
        else
        {
            System.out.println("File Not Found.");
        }

       
}

}
    


